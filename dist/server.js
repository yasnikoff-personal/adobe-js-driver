"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var net_1 = __importDefault(require("net"));
var xml_js_1 = __importDefault(require("xml-js"));
var common_1 = require("./adobe/xml/common");
var logging_1 = require("./logging");
var constants_1 = require("./common/constants");
var Server = /** @class */ (function () {
    function Server(options) {
        this.clientConnected = false;
        this.executors = [];
        this.collectedData = "";
        var opt = options || {};
        this.port = opt.port || constants_1.DEFAULT_PORT;
        this.logger = opt.logger || logging_1.getDefaultLogger();
    }
    Server.prototype.log = function (level, msg) {
        this.logger.log(level, msg);
    };
    Object.defineProperty(Server.prototype, "isRunning", {
        get: function () {
            return !!this.srv;
        },
        enumerable: true,
        configurable: true
    });
    Server.prototype.start = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                if (this.isRunning)
                    return [2 /*return*/, Promise.resolve()];
                this.srv = net_1.default.createServer(this.listener.bind(this));
                this.srv.on("close", this.onServerClose.bind(this));
                this.srv.on("error", this.onServerError.bind(this));
                this.srv.listen(this.port);
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        // promise will be resolved on the (first and the only) client connection
                        _this._clientConnectedResolve = resolve;
                        _this._clientConnectedReject = reject;
                    })];
            });
        });
    };
    Server.prototype.onServerClose = function () { };
    Server.prototype.onServerError = function (e) {
        this._clientConnectedReject && this._clientConnectedReject(e);
        // this.reject(e);
    };
    Server.prototype.getNextCallbacks = function () {
        var callbacks = this.executors.shift();
        if (!callbacks)
            throw new Error("no callbacks are queued");
        return callbacks;
    };
    Server.prototype.resolveNextCommandPromise = function (data) {
        // console.log("resolving with data:");
        // console.dir(data);
        var receivedObject = xml_js_1.default.xml2js(data, {
            compact: true
        });
        var normalizedObject = common_1.normalize(receivedObject);
        // console.dir({ received: receivedObject });
        // console.dir({ normalized: normalizedObject });
        this.getNextCallbacks().resolve(normalizedObject);
    };
    Server.prototype.rejectNextCommandPromise = function (e) {
        this.getNextCallbacks().reject(e);
    };
    Server.prototype.rejectAllRemainingCommandPromises = function () {
        // FIXME: implement
    };
    Server.prototype.close = function () {
        // this.log("closing");
        if (this.isRunning && this.srv)
            this.srv.close();
    };
    Server.prototype.listener = function (socket) {
        var _this = this;
        if (this.clientConnected)
            throw new Error("client attempted to connect second time");
        this.sock = socket;
        // socket.setEncoding("utf-8");
        socket.on("data", function (data) {
            _this.log("debug", "received from photoshop: " + data);
            var chunks = (_this.collectedData + data.toString("utf-8")).split("\n\n");
            _this.collectedData = chunks.pop() || "";
            for (var _i = 0, chunks_1 = chunks; _i < chunks_1.length; _i++) {
                var chunk = chunks_1[_i];
                _this.resolveNextCommandPromise(chunk);
            }
        });
        socket.on("end", function () {
            _this.log("debug", "client disconnected");
            // if (this.srv) this.srv.close();
            _this.close();
        });
        socket.on("error", function (e) {
            _this.rejectNextCommandPromise(e);
            _this.close(); // do not keep the other side waiting forever
        });
        if (!this._clientConnectedResolve)
            throw new Error("clientConnectionResolve is not set");
        this._clientConnectedResolve && this._clientConnectedResolve();
    };
    Server.prototype.command = function (cmd) {
        return __awaiter(this, void 0, void 0, function () {
            var cmdXml, socket, resultPromise, res;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        cmdXml = xml_js_1.default.js2xml({ command: cmd }, { compact: true });
                        this.log("debug", "sending command: " + cmdXml);
                        if (this.sock) {
                            this.sock.write(cmdXml + "\n");
                        }
                        else {
                            throw new Error("no connection to the app");
                        }
                        socket = this.sock;
                        resultPromise = new Promise(function (resolve, reject) {
                            if (!socket)
                                reject(new Error("no connection to the app"));
                            _this.executors.push({ resolve: resolve, reject: reject });
                        });
                        return [4 /*yield*/, resultPromise];
                    case 1:
                        res = (_a.sent());
                        // console.log(`result promise resolved with "${res}"`);
                        return [2 /*return*/, res];
                }
            });
        });
    };
    return Server;
}());
exports.Server = Server;
//# sourceMappingURL=server.js.map