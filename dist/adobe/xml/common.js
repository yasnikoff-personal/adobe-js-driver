"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function normalize(obj) {
    switch (typeof obj) {
        case "object": {
            for (var prop in obj) {
                var val = obj[prop];
                // convert key:{_text:value} to key:value
                if (val._text !== undefined) {
                    // fixme: presumably improve performance by encoding value type in xml.
                    // Do not try to convert to number every value.
                    obj[prop] = convertToTyped(val._text);
                }
                else {
                    obj[prop] = normalize(convertToTyped(val));
                }
            }
            break;
        }
        case "boolean":
        case "number":
            break;
        case "string":
            obj = convertToTyped(obj);
    }
    return obj;
}
exports.normalize = normalize;
function convertToTyped(value) {
    var result = Number(value.toString());
    if (!isNaN(result))
        return result;
    if (value === "true")
        return true;
    if (value === "false")
        return false;
    if (value === "undefined")
        return undefined;
    return value;
}
exports.convertToTyped = convertToTyped;
//# sourceMappingURL=common.js.map