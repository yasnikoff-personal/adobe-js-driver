import * as core from "./_core";
export declare const defaultOptions: Options;
export declare type Options = {
    path: string;
    namespace: string;
};
export declare type Result = any;
export declare class Command implements core.Command<Options, Result> {
    kind: core.Kind;
    options: Options;
    constructor(options?: Partial<Options>);
    default(): Options;
}
export declare class LoadScriptError extends Error {
    readonly cause?: Error | undefined;
    static readonly msg = "can't load script'";
    private static messageWithData;
    readonly info: any;
    constructor(options?: Options, cause?: Error | undefined);
}
