export interface ResultOrError<R = null> {
    result?: R;
    error?: Error;
}
export declare enum Kind {
    Empty = "",
    Close = "close",
    LoadScript = "load_script",
    Execute = "exec",
    Echo = "echo",
    OpenDoc = "open_doc",
    CloseDoc = "close_doc"
}
export declare function getType(typeName: string): Kind;
export declare const kindFieldName = "kind";
export declare const optionsFieldName = "options";
export interface Command<Options = null, Result = null> {
    readonly [kindFieldName]: Kind;
    readonly [optionsFieldName]?: Options;
    result?: ResultOrError<Result>;
}
