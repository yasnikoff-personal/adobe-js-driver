"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core = __importStar(require("./_core"));
exports.defaultOptions = {
    path: "script.js",
    namespace: "script"
};
// Load Script
var Command = /** @class */ (function () {
    function Command(options) {
        this.kind = core.Kind.LoadScript;
        this.options = __assign({}, this.default(), options);
    }
    Command.prototype.default = function () {
        return exports.defaultOptions;
    };
    return Command;
}());
exports.Command = Command;
var LoadScriptError = /** @class */ (function (_super) {
    __extends(LoadScriptError, _super);
    function LoadScriptError(options, cause) {
        var _this = _super.call(this, LoadScriptError.messageWithData(options) + ": " + cause) || this;
        _this.cause = cause;
        _this.info = { options: options };
        return _this;
    }
    LoadScriptError.messageWithData = function (options) {
        var path = options && options.path ? " \"" + options.path + "\"" : "";
        return "" + LoadScriptError.msg + path;
    };
    LoadScriptError.msg = "can't load script'";
    return LoadScriptError;
}(Error));
exports.LoadScriptError = LoadScriptError;
//# sourceMappingURL=load_script.js.map