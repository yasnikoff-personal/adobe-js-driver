import * as core from "./_core";
export declare type Options = {
    path: string;
};
export declare class Command implements core.Command<Options> {
    readonly options: Options;
    kind: core.Kind;
    constructor(options: Options);
}
export declare class OpenDocError extends Error {
    readonly options?: Options | undefined;
    readonly cause?: Error | undefined;
    private static errMsg;
    constructor(options?: Options | undefined, cause?: Error | undefined);
}
