import * as core from "./_core";
export declare class Options<T> {
    readonly value: T;
    constructor(value: T);
}
export declare class Command<T = null> implements core.Command<Options<T>, T> {
    readonly options: Options<T>;
    kind: core.Kind;
    constructor(options: Options<T>);
}
