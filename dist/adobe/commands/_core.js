"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Kind;
(function (Kind) {
    Kind["Empty"] = "";
    Kind["Close"] = "close";
    Kind["LoadScript"] = "load_script";
    Kind["Execute"] = "exec";
    Kind["Echo"] = "echo";
    Kind["OpenDoc"] = "open_doc";
    Kind["CloseDoc"] = "close_doc";
})(Kind = exports.Kind || (exports.Kind = {}));
function getType(typeName) {
    switch (typeName) {
        case "":
            return Kind.Empty;
        case "close":
            return Kind.Close;
        case "load_script":
            return Kind.LoadScript;
        case "exec":
            return Kind.Execute;
        case "echo":
            return Kind.Echo;
        case "open_doc":
            return Kind.OpenDoc;
        case "close_doc":
            return Kind.CloseDoc;
        default:
            throw new Error("unknown command type: \"" + typeName + "\"");
    }
}
exports.getType = getType;
exports.kindFieldName = "kind";
exports.optionsFieldName = "options";
//# sourceMappingURL=_core.js.map