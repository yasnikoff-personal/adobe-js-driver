"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core = __importStar(require("./_core"));
// Open Document
var Command = /** @class */ (function () {
    function Command(options) {
        this.options = options;
        this.kind = core.Kind.OpenDoc;
    }
    return Command;
}());
exports.Command = Command;
var OpenDocError = /** @class */ (function (_super) {
    __extends(OpenDocError, _super);
    function OpenDocError(options, cause) {
        var _this = _super.call(this, OpenDocError.errMsg(options) + ":" + cause) || this;
        _this.options = options;
        _this.cause = cause;
        return _this;
    }
    OpenDocError.errMsg = function (options) {
        var path = options && options.path ? " '" + options.path + "'" : "";
        return "can't open document " + path;
    };
    return OpenDocError;
}(Error));
exports.OpenDocError = OpenDocError;
//# sourceMappingURL=doc_open.js.map