"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core = __importStar(require("./_core"));
var loadScript = __importStar(require("./load_script"));
exports.defaultExecuteOptions = {
    namespace: loadScript.defaultOptions.namespace,
    functionName: "main",
    functionArgs: {}
};
// Execute Script
var Command = /** @class */ (function () {
    function Command(options) {
        this.kind = core.Kind.Execute;
        this.options = __assign({}, this.default(), options);
    }
    Command.prototype.default = function () {
        return exports.defaultExecuteOptions;
    };
    return Command;
}());
exports.Command = Command;
//# sourceMappingURL=execute.js.map