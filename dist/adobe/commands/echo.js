"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core = __importStar(require("./_core"));
// Echo value
var Options = /** @class */ (function () {
    function Options(value) {
        this.value = value;
    }
    return Options;
}());
exports.Options = Options;
// echo command is kind of stepping stone for development:
// its primary purpose is to make sure we can pass values back and forth unchanged.
var Command = /** @class */ (function () {
    function Command(options) {
        this.options = options;
        this.kind = core.Kind.Echo;
    }
    return Command;
}());
exports.Command = Command;
//# sourceMappingURL=echo.js.map