import * as core from "./_core";
export declare type Options = {
    namespace: string;
    functionName: string;
    functionArgs: any;
};
export declare const defaultExecuteOptions: {
    namespace: string;
    functionName: string;
    functionArgs: {};
};
export declare class Command implements core.Command<Options> {
    kind: core.Kind;
    options: Options;
    constructor(options?: Partial<Options>);
    default(): Options;
}
