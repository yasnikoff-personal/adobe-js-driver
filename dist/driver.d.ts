import { Logger } from "./logging";
import * as commands from "./adobe/commands";
import { Command } from "./adobe/commands/_core";
export declare enum App {
    Photoshop = "Photoshop",
    Illustrator = "Illustrator"
}
export interface IAppConfig {
    executable: string;
}
export interface IConfig {
    get(app: App): Promise<IAppConfig>;
    logger: Logger;
}
export declare class DefaultConfig implements IConfig {
    private config?;
    readonly logger: Logger;
    constructor();
    get(app: App): Promise<IAppConfig>;
}
export declare type configProvider = () => Promise<IConfig>;
export declare class Driver {
    readonly app: App;
    readonly config: IConfig;
    logger: Logger;
    static createDefault(app: App): Driver;
    private static wrapper;
    private server;
    private isRunning;
    constructor(app: App, config: IConfig);
    start(): Promise<void>;
    stop(): Promise<void>;
    command<C extends Command<any, R>, R = object>(cmd: C): Promise<R>;
    echo<T>(value: T): Promise<T>;
    loadScript(options?: Partial<commands.LoadScriptOptions>): Promise<commands.LoadScriptResult>;
    execute<R>(options?: Partial<commands.ExecuteOptions>): Promise<R>;
    openDoc(path: string): Promise<boolean>;
    closeDoc(): Promise<boolean>;
}
export declare const photoshop: Driver;
export declare const illustrator: Driver;
