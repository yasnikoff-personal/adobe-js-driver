"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
// Run socket server on node and client app on Adobe application.
// Server waits for exactly one connection from the application
// and then uses simple ad hoc protocol to tell the application
// what script to run and what arguments to use.
var constants_1 = require("./common/constants");
exports.CONFIG_ENV_VAR_NAME = constants_1.CONFIG_ENV_VAR_NAME;
__export(require("./driver"));
var constants_2 = require("./common/constants");
exports.DEFAULT_PORT = constants_2.DEFAULT_PORT;
var runner_1 = require("./runner");
exports.runScript = runner_1.runScript;
//# sourceMappingURL=index.js.map