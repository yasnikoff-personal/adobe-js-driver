import { App } from "./driver";
export { App } from "./driver";
export interface Options<Args = any> {
    script: string;
    function?: string;
    args: Args;
    app: App;
}
export declare const defaultOptions: Options;
export declare function runScript<R = any>(options: Partial<Options>): Promise<R>;
export default runScript;
