"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var child_process = __importStar(require("child_process"));
var path_1 = __importDefault(require("path"));
var util_1 = require("util");
var verror_1 = require("verror");
var logging_1 = require("./logging");
var filesystem_1 = require("./filesystem");
var constants_1 = require("./common/constants");
var commands = __importStar(require("./adobe/commands"));
var server_1 = require("./server");
var exec = util_1.promisify(child_process.exec);
var App;
(function (App) {
    App["Photoshop"] = "Photoshop";
    App["Illustrator"] = "Illustrator";
})(App = exports.App || (exports.App = {}));
function AppByName(name) {
    switch (name.toLowerCase()) {
        case "photoshop":
            return App.Photoshop;
        case "illustrator":
            return App.Illustrator;
        default:
            return null;
    }
}
// Driver configuration from file
var FileConfig = /** @class */ (function () {
    function FileConfig(path) {
        this.path = path;
        this.apps = new Map();
        this.loaded = false;
    }
    // load configuration from file.
    FileConfig.prototype.load = function () {
        return __awaiter(this, void 0, void 0, function () {
            var config, _a, _b, e_1, appName, app;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _c.trys.push([0, 2, , 3]);
                        _b = (_a = JSON).parse;
                        return [4 /*yield*/, filesystem_1.readFile(this.path, "utf-8")];
                    case 1:
                        config = _b.apply(_a, [_c.sent()]);
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _c.sent();
                        throw new verror_1.VError(e_1, "can't load config from file '" + path_1.default + "'");
                    case 3:
                        try {
                            for (appName in config) {
                                app = AppByName(appName);
                                if (app) {
                                    this.apps.set(app, config[appName]);
                                }
                            }
                        }
                        catch (e) {
                            throw new verror_1.VError(e, "can't generate config from data " + util_1.inspect(config));
                        }
                        this.loaded = true;
                        return [2 /*return*/];
                }
            });
        });
    };
    FileConfig.prototype.get = function (app) {
        return __awaiter(this, void 0, void 0, function () {
            var config;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.loaded) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.load()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        config = this.apps.get(app);
                        if (config) {
                            return [2 /*return*/, config];
                        }
                        else {
                            throw new Error("can't find config for " + app.toString());
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    return FileConfig;
}());
var DefaultConfig = /** @class */ (function () {
    function DefaultConfig() {
        this.logger = logging_1.getDefaultLogger();
    }
    DefaultConfig.prototype.get = function (app) {
        return __awaiter(this, void 0, void 0, function () {
            var configFile;
            return __generator(this, function (_a) {
                if (!this.config) {
                    configFile = process.env[constants_1.CONFIG_ENV_VAR_NAME];
                    this.config = new FileConfig(configFile);
                }
                return [2 /*return*/, this.config.get(app)];
            });
        });
    };
    return DefaultConfig;
}());
exports.DefaultConfig = DefaultConfig;
// running script with photoshop always throws for some reason
var Driver = /** @class */ (function () {
    function Driver(app, config) {
        this.app = app;
        this.config = config;
        this.isRunning = false;
        this.logger = config.logger;
        this.server = new server_1.Server({ logger: this.logger });
    }
    Driver.createDefault = function (app) {
        return new Driver(app, new DefaultConfig());
    };
    // ------ Driver starting and stopping
    Driver.prototype.start = function () {
        return __awaiter(this, void 0, void 0, function () {
            var appConfig, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.isRunning)
                            return [2 /*return*/];
                        return [4 /*yield*/, this.config.get(this.app)];
                    case 1:
                        appConfig = _a.sent();
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, exec("\"" + appConfig.executable + "\" -r \"" + Driver.wrapper + "\"")];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        e_2 = _a.sent();
                        return [3 /*break*/, 5];
                    case 5: return [4 /*yield*/, this.server.start()];
                    case 6:
                        _a.sent();
                        this.isRunning = true;
                        return [2 /*return*/];
                }
            });
        });
    };
    Driver.prototype.stop = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.isRunning)
                            return [2 /*return*/, Promise.resolve()];
                        return [4 /*yield*/, this.server.command(new commands.Close())];
                    case 1:
                        _a.sent();
                        this.server.close();
                        this.isRunning = false;
                        return [2 /*return*/];
                }
            });
        });
    };
    // ----- Basic commands
    Driver.prototype.command = function (cmd) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.server.command(cmd)];
                    case 1:
                        result = _a.sent();
                        if (result.error)
                            throw result.error;
                        if (result.result)
                            return [2 /*return*/, result.result];
                        console.dir(result);
                        throw new Error("RunResult expected but got " + util_1.inspect(result));
                }
            });
        });
    };
    Driver.prototype.echo = function (value) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.command(new commands.Echo({ value: value }))];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    // -------- Script loading and execution
    // T - type of the value the script evaluates to
    Driver.prototype.loadScript = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.command(new commands.LoadScript(options))];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    Driver.prototype.execute = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.command(new commands.Execute(options))];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    // --- Document opening, saving  and closing
    Driver.prototype.openDoc = function (path) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.command(new commands.OpenDoc({ path: path }))];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    Driver.prototype.closeDoc = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.command(new commands.CloseDoc())];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    Driver.wrapper = path_1.default.normalize(__dirname + "/../dist/script_wrapper.js");
    return Driver;
}());
exports.Driver = Driver;
// stderr and stdout are not collected:
// ------ default drivers for evert Application
exports.photoshop = Driver.createDefault(App.Photoshop);
exports.illustrator = Driver.createDefault(App.Illustrator);
//# sourceMappingURL=driver.js.map