import * as commands from "./adobe/commands/_core";
import { Logger } from "./logging";
export interface Options {
    logger?: Logger;
    port?: number;
}
export declare class Server {
    private srv?;
    private sock?;
    private clientConnected;
    private executors;
    private _clientConnectedResolve?;
    private _clientConnectedReject?;
    private collectedData;
    private logger;
    readonly port: number;
    constructor(options?: Options);
    private log;
    readonly isRunning: boolean;
    start(): Promise<void | {}>;
    private onServerClose;
    private onServerError;
    private getNextCallbacks;
    private resolveNextCommandPromise;
    private rejectNextCommandPromise;
    private rejectAllRemainingCommandPromises;
    close(): void;
    private listener;
    command<T extends commands.Command<any, R>, R = object>(cmd: T): Promise<commands.ResultOrError<R>>;
}
