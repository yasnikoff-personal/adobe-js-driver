(function () {
  'use strict';

  var Kind;
  (function (Kind) {
      Kind["Empty"] = "";
      Kind["Close"] = "close";
      Kind["LoadScript"] = "load_script";
      Kind["Execute"] = "exec";
      Kind["Echo"] = "echo";
      Kind["OpenDoc"] = "open_doc";
      Kind["CloseDoc"] = "close_doc";
  })(Kind || (Kind = {}));
  function getType(typeName) {
      switch (typeName) {
          case "":
              return Kind.Empty;
          case "close":
              return Kind.Close;
          case "load_script":
              return Kind.LoadScript;
          case "exec":
              return Kind.Execute;
          case "echo":
              return Kind.Echo;
          case "open_doc":
              return Kind.OpenDoc;
          case "close_doc":
              return Kind.CloseDoc;
          default:
              throw new Error("unknown command type: \"" + typeName + "\"");
      }
  }
  var kindFieldName = "kind";
  var optionsFieldName = "options";

  function convertToTyped(value) {
      var result = Number(value.toString());
      if (!isNaN(result))
          return result;
      if (value === "true")
          return true;
      if (value === "false")
          return false;
      if (value === "undefined")
          return undefined;
      return value;
  }

  const functionString = "[Function]";
  const DEFAULT_PORT = 3584;

  function js2xml(src, tagName) {
      var name = new QName(tagName);
      var result = new XML("<" + name + "></" + name + ">");
      switch (typeof src) {
          case "object":
              {
                  for (var prop in src) {
                      var child = js2xml(src[prop], prop);
                      result.appendChild(child);
                  }
              }
              break;
          case "string":
          case "number":
          case "undefined":
              result = new XML("<" + name + ">" + src + "</" + name + ">");
              break;
          case "boolean":
              result = new XML("<" + name + ">" + (src ? "true" : "false") + "</" + name + ">");
              break;
          case "function":
              result = new XML("<" + name + ">" + functionString + "</" + name + ">");
              // alert(`${result.toXMLString()}`);
              break;
          default:
              throw new Error("unknown type: " + typeof src);
      }
      return result;
  }
  function xml2js(src) {
      switch (src.nodeKind()) {
          case "list":
          case "element": {
              var result = {};
              var childrenCount = src.length();
              for (var i = 0; i < childrenCount; i++) {
                  var child = src[i];
                  result[child.name().localName] = xml2js(child.children());
              }
              return result;
          }
          case "text": {
              return convertToTyped(src.toString());
          }
          case "comment":
              break;
          case "attribute":
          case "processing instruction":
              throw new Error("invalid node type:" + src.nodeKind());
      }
  }

  /*! *****************************************************************************
  Copyright (c) Microsoft Corporation. All rights reserved.
  Licensed under the Apache License, Version 2.0 (the "License"); you may not use
  this file except in compliance with the License. You may obtain a copy of the
  License at http://www.apache.org/licenses/LICENSE-2.0

  THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
  WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
  MERCHANTABLITY OR NON-INFRINGEMENT.

  See the Apache Version 2.0 License for specific language governing permissions
  and limitations under the License.
  ***************************************************************************** */
  /* global Reflect, Promise */

  var extendStatics = function(d, b) {
      extendStatics = Object.setPrototypeOf ||
          ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
          function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
      return extendStatics(d, b);
  };

  function __extends(d, b) {
      extendStatics(d, b);
      function __() { this.constructor = d; }
      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  }

  var LoadScriptError = /** @class */ (function (_super) {
      __extends(LoadScriptError, _super);
      function LoadScriptError(options, cause) {
          var _this = _super.call(this, LoadScriptError.messageWithData(options) + ": " + cause) || this;
          _this.cause = cause;
          _this.info = { options: options };
          return _this;
      }
      LoadScriptError.messageWithData = function (options) {
          var path = options && options.path ? " \"" + options.path + "\"" : "";
          return "" + LoadScriptError.msg + path;
      };
      LoadScriptError.msg = "can't load script'";
      return LoadScriptError;
  }(Error));

  var OpenDocError = /** @class */ (function (_super) {
      __extends(OpenDocError, _super);
      function OpenDocError(options, cause) {
          var _this = _super.call(this, OpenDocError.errMsg(options) + ":" + cause) || this;
          _this.options = options;
          _this.cause = cause;
          return _this;
      }
      OpenDocError.errMsg = function (options) {
          var path = options && options.path ? " '" + options.path + "'" : "";
          return "can't open document " + path;
      };
      return OpenDocError;
  }(Error));

  var CommandData = /** @class */ (function () {
      function CommandData(kind, options, xml) {
          this.kind = kind;
          this.options = options;
          this.xml = xml;
      }
      CommandData.parse = function (xml) {
          var data = new XML(xml);
          var optionsObject = xml2js(data.child(optionsFieldName));
          var options = optionsObject
              ? optionsObject[optionsFieldName]
              : undefined; //root tag is 'options'
          var kind = getType(data.child(kindFieldName).toString());
          return new CommandData(kind, options, data);
      };
      return CommandData;
  }());
  var Connection = /** @class */ (function () {
      function Connection(port) {
          this.port = port;
          this.socket = new Socket();
      }
      Connection.prototype.open = function () {
          return this.socket.open("localhost:" + this.port, "UTF-8");
      };
      Connection.prototype.close = function () {
          this.socket.close();
      };
      Connection.prototype.connected = function () {
          return this.socket.connected;
      };
      Connection.prototype.writeRawText = function (text) {
          if (this.connected()) {
              this.socket.writeln(text);
              this.socket.write("\n");
          }
      };
      Connection.prototype.readRawText = function () {
          return this.socket.readln();
      };
      Connection.prototype.writeXml = function (obj, rootTag) {
          var xmlString = js2xml(obj, rootTag).toXMLString();
          this.writeRawText(xmlString);
      };
      Connection.prototype.send = function (obj) {
          this.writeXml(obj, "result");
      };
      Connection.prototype.sendError = function (e) {
          this.writeXml("" + e.toSource(), "error");
          // connection.close(); // ready for next commands. driver should close the connection in catch block.
      };
      return Connection;
  }());
  function handleError(e, msg) {
      connection.close();
      alert("Error: " + e + "\nline: " + e.line);
      if (msg)
          alert("" + msg);
  }
  // fixme: provide configuration options
  var connection = new Connection(DEFAULT_PORT);
  var scripts = {};
  try {
      if (connection.open()) {
          while (connection.connected()) {
              try {
                  var receivedText = connection.readRawText();
                  var command = CommandData.parse(receivedText);
                  if (command.kind === Kind.Close) {
                      connection.send("");
                      break;
                  }
                  switch (command.kind) {
                      case Kind.Empty: {
                          break;
                      }
                      case Kind.Echo: {
                          try {
                              var received = command.options.value; // root tag: value
                              // alert(`received: ${received}`);
                              connection.send(received); // strip root tag
                          }
                          catch (e) {
                              connection.sendError(e);
                          }
                          break;
                      }
                      case Kind.LoadScript: {
                          var options = void 0;
                          try {
                              // const script = command.xml.child("path").toString();
                              options = command.options;
                              var script = options.path;
                              // const name = command.xml.child("namespace").toString();
                              var name = options.namespace;
                              var scriptContent = $.evalFile(new File(script));
                              scripts[name] = scriptContent;
                              connection.send(scriptContent);
                          }
                          catch (e) {
                              connection.sendError(new LoadScriptError(options, e));
                          }
                          break;
                      }
                      case Kind.Execute: {
                          try {
                              var options = command.options;
                              var scriptName = options.namespace;
                              var funcName = options.functionName;
                              var args = options.functionArgs;
                              var result = void 0;
                              if (args) {
                                  result = scripts[scriptName][funcName](args);
                              }
                              else {
                                  result = scripts[scriptName][funcName]();
                              }
                              connection.send(result);
                          }
                          catch (e) {
                              connection.sendError(e);
                          }
                          break;
                      }
                      case Kind.OpenDoc: {
                          var options = void 0;
                          try {
                              options = command.options;
                              var path = options.path;
                              app.load(new File(path));
                              if (app.activeDocument) {
                                  connection.send(true);
                              }
                              else {
                                  connection.send(false);
                              }
                          }
                          catch (e) {
                              connection.sendError(new OpenDocError(options, e));
                          }
                          break;
                      }
                      case Kind.CloseDoc: {
                          try {
                              if (app.activeDocument) {
                                  app.activeDocument.close(SaveOptions.DONOTSAVECHANGES);
                              }
                              connection.send(true);
                          }
                          catch (e) {
                              connection.sendError(e);
                          }
                          break;
                      }
                  }
              }
              catch (e) {
                  handleError(e);
                  connection.sendError(e);
                  break;
              }
          }
          connection.close();
      }
  }
  catch (e) {
      //$.write(e); // does not work
      handleError(e);
  }

}());
