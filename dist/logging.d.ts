import winston from "winston";
export declare type Logger = winston.Logger;
export declare function getDefaultLogger(): Logger;
