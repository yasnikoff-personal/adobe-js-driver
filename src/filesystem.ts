import { promisify } from "util";
import fs from "fs";

export let readFile = promisify(fs.readFile);
