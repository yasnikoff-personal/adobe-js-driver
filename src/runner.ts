import { Driver, App } from "./driver";
export { App } from "./driver";

export interface Options<Args = any> {
  script: string;
  function?: string;
  args: Args;
  app: App;
}
export const defaultOptions: Options = {
  app: App.Photoshop,
  script: "index.js",
  function: "main",
  args: {}
};

// Run function @options.functionName from script @options.name
export async function runScript<R = any>(
  options: Partial<Options>
): Promise<R> {
  const opt: Options = { ...defaultOptions, ...options };
  const driver = Driver.createDefault(opt.app);
  await driver.start();
  let result: R;
  try {
    await driver.loadScript({ path: options.script });

    result = await driver.execute<R>({
      functionName: opt.function,
      functionArgs: opt.args
    });
  } finally {
    await driver.stop();
  }
  return result;
}

export default runScript;
