// import jest from "jest";
import { photoshop, Driver } from "../driver";
import path from "path";

import { functionString } from "../common/constants";

const scriptsFolder = path.join(__dirname, "scripts");
const pdsFolder = path.join(__dirname, "psd");

// test run in photoshop so they can take longer time.
// sometimes timeout should accommodate photoshop starting time
const testTimeout = 4000;

// loading psd docs takes more time
const testTimeoutForDocs = testTimeout * 3;

photoshop.logger.level = "info";

describe("ScriptRunner", () => {
  let driver: Driver;
  interface TestFixture {
    driver: Driver;
  }

  async function getTestFixture(): Promise<TestFixture> {
    await photoshop.start();
    return { driver: photoshop };
  }

  // For performance reasons setup is made only once.
  // Change to "beforeEach" and "afterEach" for better tests isolation.
  beforeAll(async () => {
    driver = (await getTestFixture()).driver;
  });
  afterAll(async () => {
    await driver.stop();
  });

  //
  // ----------- Echo command
  //
  describe("Echo command returns its argument unchanged", () => {
    // echo command is kind of stepping stone for development:
    // its primary purpose is to make sure we can pass values back and forth unchanged.
    const testCases = [
      { name: "one word", value: "hello" },
      { name: "string with spaces", value: "string with spaces" },
      { name: "number", value: 6 },
      { name: "shallow object with one field", value: { key: "value" } },
      {
        name: "shallow object with two fields",
        value: { key1: "value1", key2: "value2" }
      },
      { name: "nested object", value: { root: { level1: "hello" } } },
      {
        name: "multilevel object",
        value: { root1: { level11: "hello11" }, root2: "hello2" }
      }
    ];

    for (const t of testCases) {
      test(
        t.name,
        async () => {
          const result = await driver.echo(t.value);
          expect(result).toEqual(t.value);
        },
        testTimeout
      );
    }
  });
  //
  // ----------- Load command
  //
  describe("Load command returns script's evaluation result of type", () => {
    const testCases = [
      { name: "number", script: "returns_5.adobe.js", expected: 5 },
      {
        name: "function",
        script: "returns_function.adobe.js",
        expected: functionString
      },
      {
        name: "object with functions",
        script: "returns_object_with_functions.adobe.js",
        expected: { add: functionString, subtract: functionString }
      }
    ];

    for (const t of testCases) {
      t.script = path.join(scriptsFolder, t.script);
      test(
        t.name,
        async () => {
          const result = await driver.loadScript({
            path: t.script,
            namespace: "testScript"
          });
          expect(result).toEqual(t.expected);
        },
        testTimeout
      );
    }
  });
  test('Load command can be run without "namespace" parameter', async () => {
    const script = path.join(
      scriptsFolder,
      "returns_object_with_functions.adobe.js"
    );
    const result = await driver.loadScript({
      path: script
    });
    expect(result).toEqual({
      add: functionString,
      subtract: functionString
    });
  });
  describe("Load command throws informative error", () => {
    test("when script file is not found", async () => {
      const path = "no_such_dir/no_such_script.js";
      try {
        await driver.loadScript({ path });
      } catch (e) {
        const errMsg = `${e}`;
        expect(errMsg).toContain(`can't load script`);
        expect(errMsg).toContain(path);
      }
    });
  });
  describe("exec command with namespace", () => {
    const testCases = [
      {
        name: "function without args",
        scriptFile: "test_functions.adobe.js",
        functionName: "funcWithoutArgs",
        args: {},
        expectedResult: "hello"
      },
      {
        name: "function with args object",
        scriptFile: "test_functions.adobe.js",
        functionName: "funcWithArgsObject",
        args: { x: 2, y: 3 },
        expectedResult: 5
      }
    ];

    for (const t of testCases) {
      t.scriptFile = path.join(scriptsFolder, t.scriptFile);

      test(t.name, async () => {
        const namespace = "script1";
        const loadResult: any = await driver.loadScript({
          path: t.scriptFile,
          namespace
        });
        expect(loadResult).toHaveProperty(t.functionName, functionString);
        const execResult: any = await driver.execute({
          namespace,
          functionName: t.functionName,
          functionArgs: t.args
        });

        expect(execResult).toEqual(t.expectedResult);
      });
    }
  });
  describe("Exec command can be run without namespace parameter", () => {
    const testCases = [
      {
        name: "function without args",
        scriptFile: "test_functions.adobe.js",
        functionName: "funcWithoutArgs",
        args: {},
        expectedResult: "hello"
      },
      {
        name: "function with args object",
        scriptFile: "test_functions.adobe.js",
        functionName: "funcWithArgsObject",
        args: { x: 2, y: 3 },
        expectedResult: 5
      }
    ];

    for (const t of testCases) {
      t.scriptFile = path.join(scriptsFolder, t.scriptFile);
      test(t.name, async () => {
        const loadResult: any = await driver.loadScript({
          path: t.scriptFile
        });
        expect(loadResult).toHaveProperty(t.functionName, functionString);
        const execResult: any = await driver.execute({
          functionName: t.functionName,
          functionArgs: t.args
        });

        expect(execResult).toEqual(t.expectedResult);
      });
    }
  });
  //
  // ----------- OpenDoc command
  //
  describe("openDoc command", () => {
    const testCases = [{ name: "valid existing psd file", path: "test.psd" }];

    for (const t of testCases) {
      t.path = path.join(pdsFolder, t.path);
      test(
        t.name,
        async () => {
          const openResult = await driver.openDoc(t.path);
          expect(openResult).toEqual(true);
          const closeResult = await driver.closeDoc();
          expect(closeResult).toEqual(true);
        },
        testTimeoutForDocs
      );
    }
  });
  describe("throws informative error", () => {
    test("when document is not found", async () => {
      const path = "no_such_doc.psd";
      try {
        await driver.openDoc(path);
        throw new Error(`test fixture expected to throw error but it didn't`);
      } catch (e) {
        const msg = `${e}`;
        expect(msg).toContain(`can't open`);
        expect(msg).toContain(path);
      }
    });
  });
});
