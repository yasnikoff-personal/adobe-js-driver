var result = {
    funcWithoutArgs: function () {
        return "hello"
    },
    funcWithArgsObject: function (args) {
        return args.x + args.y
    }
}

result