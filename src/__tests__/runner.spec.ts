import path from "path";
import run, { App } from "../runner";

describe("script runner", () => {
  describe("for Photoshop App", () => {
    test("runs script", async () => {
      const result = await run({
        app: App.Photoshop,
        script: path.join(__dirname, "scripts", "test_functions.adobe.js"),
        function: "funcWithArgsObject",
        args: { x: 2, y: 6 }
      });
      expect(result).toEqual(8);
    });
  });
});
