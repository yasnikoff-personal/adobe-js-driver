import { normalize } from "../adobe/xml/common";

describe("xml2js", () => {
  describe('converts {_text:"value"} to "value"', () => {
    test("for one level deep prop", () => {
      expect(normalize({ name: { _text: "script" } })).toStrictEqual({
        name: "script"
      });
    });
    test("for two level deep prop", () => {
      expect(normalize({ root: { name: { _text: "script" } } })).toStrictEqual({
        root: { name: "script" }
      });
    });
    test("for number", () => {
      expect(normalize(5)).toEqual(5);
    });
    test("for 'true' string", () => {
      expect(normalize("true")).toEqual(true);
    });
    test("for 'false' string", () => {
      expect(normalize("false")).toEqual(false);
    });
    test("for 'true' string in _text field", () => {
      expect(normalize({ value: { _text: "true" } })).toEqual({ value: true });
    });
    test("for 'false' string in _text field'", () => {
      expect(
        normalize({
          value: { _text: "false" }
        })
      ).toEqual({ value: false });
    });
    test("for number in _text field'", () => {
      expect(
        normalize({
          value: { _text: "7" }
        })
      ).toEqual({ value: 7 });
    });
  });
});
