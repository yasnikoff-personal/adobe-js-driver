import * as child_process from "child_process";
import path from "path";
import { promisify, inspect } from "util";

import { VError } from "verror";
import { Logger, getDefaultLogger } from "./logging";

import { readFile } from "./filesystem";
import { CONFIG_ENV_VAR_NAME } from "./common/constants";
import * as commands from "./adobe/commands";
import { Command, ResultOrError } from "./adobe/commands/_core";
import { Server } from "./server";

const exec = promisify(child_process.exec);

export enum App {
  Photoshop = "Photoshop",
  Illustrator = "Illustrator"
}
type AppName = "photoshop" | "illustrator";

function AppByName(name: AppName): App | null {
  switch (name.toLowerCase()) {
    case "photoshop":
      return App.Photoshop;
    case "illustrator":
      return App.Illustrator;
    default:
      return null;
  }
}

export interface IAppConfig {
  executable: string;
}

export interface IConfig {
  get(app: App): Promise<IAppConfig>;
  logger: Logger;
}
// Driver configuration from file
class FileConfig {
  apps: Map<App, IAppConfig> = new Map();
  private loaded = false;
  constructor(readonly path: string) {}

  // load configuration from file.
  private async load() {
    let config: any;
    try {
      config = JSON.parse(await readFile(this.path, "utf-8"));
    } catch (e) {
      throw new VError(e, `can't load config from file '${path}'`);
    }
    try {
      for (let appName in config) {
        const app = AppByName(appName as AppName);
        if (app) {
          this.apps.set(app, config[appName] as IAppConfig);
        }
      }
    } catch (e) {
      throw new VError(e, `can't generate config from data ${inspect(config)}`);
    }

    this.loaded = true;
  }

  async get(app: App): Promise<IAppConfig> {
    if (!this.loaded) {
      await this.load();
    }
    const config = this.apps.get(app);
    if (config) {
      return config;
    } else {
      throw new Error(`can't find config for ${app.toString()}`);
    }
  }
}

export class DefaultConfig implements IConfig {
  private config?: FileConfig;
  readonly logger: Logger;
  constructor() {
    this.logger = getDefaultLogger();
  }

  async get(app: App): Promise<IAppConfig> {
    if (!this.config) {
      const configFile = process.env[CONFIG_ENV_VAR_NAME] as string;
      this.config = new FileConfig(configFile);
    }
    return this.config.get(app);
  }
}
export type configProvider = () => Promise<IConfig>;

// running script with photoshop always throws for some reason

export class Driver {
  logger: Logger;

  static createDefault(app: App): Driver {
    return new Driver(app, new DefaultConfig());
  }

  private static wrapper = path.normalize(
    `${__dirname}/../dist/script_wrapper.js`
  );

  private server: Server;

  private isRunning: boolean = false;

  constructor(public readonly app: App, public readonly config: IConfig) {
    this.logger = config.logger;
    this.server = new Server({ logger: this.logger });
  }

  // ------ Driver starting and stopping

  async start() {
    if (this.isRunning) return;

    const appConfig = await this.config.get(this.app);

    try {
      await exec(`"${appConfig.executable}" -r "${Driver.wrapper}"`);
    } catch (e) {
      // running photoshop with script by child_process always throws for some reason
    }
    await this.server.start();
    this.isRunning = true;
  }
  async stop() {
    if (!this.isRunning) return Promise.resolve();

    await this.server.command(new commands.Close());
    this.server.close();

    this.isRunning = false;
  }

  // ----- Basic commands

  async command<C extends Command<any, R>, R = object>(cmd: C): Promise<R> {
    const result: ResultOrError<R> = await this.server.command<C, R>(cmd);

    if (result.error) throw result.error;
    if (result.result) return result.result;
    console.dir(result);
    throw new Error(`RunResult expected but got ${inspect(result)}`);
  }

  async echo<T>(value: T): Promise<T> {
    return await this.command<commands.Echo<T>, T>(
      new commands.Echo<T>({ value })
    );
  }

  // -------- Script loading and execution

  // T - type of the value the script evaluates to
  async loadScript(
    options?: Partial<commands.LoadScriptOptions>
  ): Promise<commands.LoadScriptResult> {
    const result = await this.command<
      commands.LoadScript,
      commands.LoadScriptResult
    >(new commands.LoadScript(options));
    return result;
  }
  async execute<R>(options?: Partial<commands.ExecuteOptions>): Promise<R> {
    return await this.command<commands.Execute, R>(
      new commands.Execute(options)
    );
  }

  // --- Document opening, saving  and closing

  async openDoc(path: string): Promise<boolean> {
    return await this.command<commands.OpenDoc, boolean>(
      new commands.OpenDoc({ path })
    );
  }
  async closeDoc(): Promise<boolean> {
    return await this.command<commands.CloseDoc, boolean>(
      new commands.CloseDoc()
    );
  }
}
// stderr and stdout are not collected:

// ------ default drivers for evert Application

export const photoshop = Driver.createDefault(App.Photoshop);
export const illustrator = Driver.createDefault(App.Illustrator);
