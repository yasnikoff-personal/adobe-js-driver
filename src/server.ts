import net from "net";
import xml from "xml-js";
import { normalize } from "./adobe/xml/common";
import * as commands from "./adobe/commands/_core";
import { Logger, getDefaultLogger } from "./logging";
import { DEFAULT_PORT } from "./common/constants";

interface ExecutorCallbacks {
  resolve: (data: any) => void;
  reject: (e: Error) => void;
}

export interface Options {
  logger?: Logger;
  port?: number;
}

export class Server {
  private srv?: net.Server;
  private sock?: net.Socket;
  private clientConnected: boolean = false;

  private executors: Array<ExecutorCallbacks> = [];

  private _clientConnectedResolve?: () => void;
  private _clientConnectedReject?: (e: Error) => void;

  private collectedData: string = "";

  private logger: Logger;
  readonly port: number;

  constructor(options?: Options) {
    const opt = options || {};
    this.port = opt.port || DEFAULT_PORT;
    this.logger = opt.logger || getDefaultLogger();
  }

  private log(level: string, msg: string) {
    this.logger.log(level, msg);
  }

  get isRunning(): boolean {
    return !!this.srv;
  }

  async start() {
    if (this.isRunning) return Promise.resolve();

    this.srv = net.createServer(this.listener.bind(this));
    this.srv.on("close", this.onServerClose.bind(this));
    this.srv.on("error", this.onServerError.bind(this));
    this.srv.listen(this.port);

    return new Promise((resolve, reject) => {
      // promise will be resolved on the (first and the only) client connection
      this._clientConnectedResolve = resolve;
      this._clientConnectedReject = reject;
    });
  }

  private onServerClose() {}

  private onServerError(e: Error) {
    this._clientConnectedReject && this._clientConnectedReject(e);
    // this.reject(e);
  }

  private getNextCallbacks(): ExecutorCallbacks {
    const callbacks = this.executors.shift();
    if (!callbacks) throw new Error(`no callbacks are queued`);
    return callbacks;
  }

  private resolveNextCommandPromise(data: string) {
    // console.log("resolving with data:");
    // console.dir(data);
    const receivedObject: any = xml.xml2js(data, {
      compact: true
    });
    const normalizedObject = normalize(receivedObject);
    // console.dir({ received: receivedObject });
    // console.dir({ normalized: normalizedObject });
    this.getNextCallbacks().resolve(normalizedObject);
  }

  private rejectNextCommandPromise(e: Error) {
    this.getNextCallbacks().reject(e);
  }

  private rejectAllRemainingCommandPromises() {
    // FIXME: implement
  }

  close() {
    // this.log("closing");
    if (this.isRunning && this.srv) this.srv.close();
  }
  private listener(socket: net.Socket) {
    if (this.clientConnected)
      throw new Error(`client attempted to connect second time`);
    this.sock = socket;
    // socket.setEncoding("utf-8");
    socket.on("data", data => {
      this.log("debug", `received from photoshop: ${data}`);
      let chunks = (this.collectedData + data.toString("utf-8")).split("\n\n");
      this.collectedData = chunks.pop() || "";
      for (let chunk of chunks) {
        this.resolveNextCommandPromise(chunk);
      }
    });
    socket.on("end", () => {
      this.log("debug", "client disconnected");
      // if (this.srv) this.srv.close();
      this.close();
    });

    socket.on("error", e => {
      this.rejectNextCommandPromise(e);
      this.close(); // do not keep the other side waiting forever
    });

    if (!this._clientConnectedResolve)
      throw new Error(`clientConnectionResolve is not set`);
    this._clientConnectedResolve && this._clientConnectedResolve();
  }

  async command<T extends commands.Command<any, R>, R = object>(
    cmd: T
  ): Promise<commands.ResultOrError<R>> {
    const cmdXml = xml.js2xml({ command: cmd }, { compact: true });
    this.log("debug", `sending command: ${cmdXml}`);
    if (this.sock) {
      this.sock.write(`${cmdXml}\n`);
    } else {
      throw new Error(`no connection to the app`);
    }
    const socket = this.sock;

    const resultPromise = new Promise<any>((resolve, reject) => {
      if (!socket) reject(new Error(`no connection to the app`));
      this.executors.push({ resolve, reject });
    });

    const res = (await resultPromise) as R;
    // console.log(`result promise resolved with "${res}"`);
    return res;
  }
}
