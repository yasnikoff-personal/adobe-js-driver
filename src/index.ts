// Run socket server on node and client app on Adobe application.
// Server waits for exactly one connection from the application
// and then uses simple ad hoc protocol to tell the application
// what script to run and what arguments to use.
export { CONFIG_ENV_VAR_NAME } from "./common/constants";
export * from "./driver";
export { DEFAULT_PORT } from "./common/constants";
export { runScript } from "./runner";
