import { CONFIG_ENV_VAR_NAME } from "../common/constants";
import {
  Kind,
  getType,
  Command,
  kindFieldName,
  optionsFieldName
} from "../adobe/commands/_core";
import * as commands from "./commands";
import { js2xml, xml2js } from "./xml/adobe";
import { DEFAULT_PORT } from "../common/constants";
import { LoadScriptError } from "./commands/load_script";
import { OpenDocError } from "./commands/doc_open";

class CommandData {
  static parse(xml: string): CommandData {
    let data = new XML(xml);

    const optionsObject: any = xml2js(data.child(optionsFieldName));
    let options: any = optionsObject
      ? optionsObject[optionsFieldName]
      : undefined; //root tag is 'options'
    let kind: Kind = getType(data.child(kindFieldName).toString());

    return new CommandData(kind, options, data);
  }

  constructor(readonly kind: Kind, readonly options: any, readonly xml: XML) {}
}

class Connection {
  private socket: Socket;
  constructor(private port: number) {
    this.socket = new Socket();
  }

  open(): boolean {
    return this.socket.open(`localhost:${this.port}`, "UTF-8");
  }
  close() {
    this.socket.close();
  }
  connected(): boolean {
    return this.socket.connected;
  }

  writeRawText(text: string) {
    if (this.connected()) {
      this.socket.writeln(text);
      this.socket.write("\n");
    }
  }

  readRawText(): string {
    return this.socket.readln();
  }

  writeXml(obj: any, rootTag: string) {
    const xmlString = js2xml(obj, rootTag).toXMLString();
    this.writeRawText(xmlString);
  }

  send(obj: any) {
    this.writeXml(obj, "result");
  }

  sendError(e: Error) {
    this.writeXml(`${e.toSource()}`, "error");
    // connection.close(); // ready for next commands. driver should close the connection in catch block.
  }
}

function handleError(e: any, msg?: string) {
  connection.close();
  alert(`Error: ${e}\nline: ${e.line}`);
  if (msg) alert(`${msg}`);
}

// fixme: provide configuration options
const connection = new Connection(DEFAULT_PORT);
const scripts = {};

try {
  if (connection.open()) {
    while (connection.connected()) {
      try {
        const receivedText = connection.readRawText();
        const command = CommandData.parse(receivedText);

        if (command.kind === Kind.Close) {
          connection.send(``);
          break;
        }

        switch (command.kind) {
          case Kind.Empty: {
            break;
          }
          case Kind.Echo: {
            try {
              const received = command.options.value; // root tag: value
              // alert(`received: ${received}`);
              connection.send(received); // strip root tag
            } catch (e) {
              connection.sendError(e);
            }

            break;
          }
          case Kind.LoadScript: {
            let options: commands.LoadScriptOptions | undefined;
            try {
              // const script = command.xml.child("path").toString();
              options = command.options as commands.LoadScriptOptions;
              const script = options.path;
              // const name = command.xml.child("namespace").toString();
              const name = options.namespace;
              const scriptContent = $.evalFile(new File(script));
              scripts[name] = scriptContent;
              connection.send(scriptContent);
            } catch (e) {
              connection.sendError(new LoadScriptError(options, e));
            }
            break;
          }
          case Kind.Execute: {
            try {
              const options = command.options as commands.ExecuteOptions;
              const scriptName = options.namespace;
              const funcName = options.functionName;
              const args = options.functionArgs;

              let result: any;
              if (args) {
                result = scripts[scriptName][funcName](args);
              } else {
                result = scripts[scriptName][funcName]();
              }
              connection.send(result);
            } catch (e) {
              connection.sendError(e);
            }
            break;
          }

          case Kind.OpenDoc: {
            let options: commands.OpenDocOptions | undefined;
            try {
              options = command.options as commands.OpenDocOptions;
              const path = options.path;
              app.load(new File(path));
              if (app.activeDocument) {
                connection.send(true);
              } else {
                connection.send(false);
              }
            } catch (e) {
              connection.sendError(new OpenDocError(options, e));
            }
            break;
          }
          case Kind.CloseDoc: {
            try {
              if (app.activeDocument) {
                app.activeDocument.close(SaveOptions.DONOTSAVECHANGES);
              }
              connection.send(true);
            } catch (e) {
              connection.sendError(e);
            }
            break;
          }
        }
      } catch (e) {
        handleError(e);
        connection.sendError(e);
        break;
      }
    }
    connection.close();
  }
} catch (e) {
  //$.write(e); // does not work
  handleError(e);
}
