export function normalize(obj: any): any {
  switch (typeof obj) {
    case "object": {
      for (const prop in obj) {
        const val: any = obj[prop];
        // convert key:{_text:value} to key:value
        if (val._text !== undefined) {
          // fixme: presumably improve performance by encoding value type in xml.
          // Do not try to convert to number every value.
          obj[prop] = convertToTyped(val._text);
        } else {
          obj[prop] = normalize(convertToTyped(val));
        }
      }
      break;
    }
    case "boolean":
    case "number":
      break;
    case "string":
      obj = convertToTyped(obj);
  }
  return obj;
}

export function convertToTyped(value: any): any {
  let result = Number(value.toString());
  if (!isNaN(result)) return result;
  if (value === "true") return true;
  if (value === "false") return false;
  if (value === "undefined") return undefined;
  return value;
}
