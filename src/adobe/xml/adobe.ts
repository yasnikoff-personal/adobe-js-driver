import { convertToTyped } from "./common";
import { functionString } from "../../common/constants";

export function js2xml(src: any, tagName: string): XML {
  const name = new QName(tagName);
  let result = new XML(`<${name}></${name}>`);
  switch (typeof src) {
    case "object":
      {
        for (const prop in src) {
          const child = js2xml(src[prop], prop);
          result.appendChild(child);
        }
      }
      break;
    case "string":
    case "number":
    case "undefined":
      result = new XML(`<${name}>${src}</${name}>`);
      break;
    case "boolean":
      result = new XML(`<${name}>${src ? "true" : "false"}</${name}>`);
      break;
    case "function":
      result = new XML(`<${name}>${functionString}</${name}>`);
      // alert(`${result.toXMLString()}`);
      break;
    default:
      throw new Error(`unknown type: ${typeof src}`);
  }
  return result;
}

export function xml2js(src: XML): any {
  switch (src.nodeKind()) {
    case "list":
    case "element": {
      let result: any = {};
      const childrenCount = src.length();
      for (let i = 0; i < childrenCount; i++) {
        const child = src[i] as XML;
        result[child.name().localName] = xml2js(child.children());
      }
      return result;
    }
    case "text": {
      return convertToTyped(src.toString());
    }
    case "comment":
      break;
    case "attribute":
    case "processing instruction":
      throw new Error(`invalid node type:${src.nodeKind()}`);
  }
}
