import typescript from 'rollup-plugin-typescript';
export default {
    input: './script_wrapper.ts',
    output: {
        file: '../../dist/script_wrapper.js',
        format: 'iife',
    },
    plugins: [
        typescript({
            tsconfig: "./tsconfig.json"
        }),
    ]
}