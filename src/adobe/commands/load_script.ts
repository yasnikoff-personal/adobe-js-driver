import * as core from "./_core";

export const defaultOptions: Options = {
  path: "script.js",
  namespace: "script"
};

export type Options = {
  path: string;
  namespace: string;
};

// fixe: more concrete type
export type Result = any;

// Load Script
export class Command implements core.Command<Options, Result> {
  kind = core.Kind.LoadScript;
  options: Options;
  constructor(options?: Partial<Options>) {
    this.options = { ...this.default(), ...options };
  }

  default(): Options {
    return defaultOptions;
  }
}
export class LoadScriptError extends Error {
  static readonly msg = `can't load script'`;
  private static messageWithData(options?: Options): string {
    const path = options && options.path ? ` "${options.path}"` : "";
    return `${LoadScriptError.msg}${path}`;
  }
  readonly info: any;
  constructor(options?: Options, readonly cause?: Error) {
    super(`${LoadScriptError.messageWithData(options)}: ${cause}`);
    this.info = { options };
  }
}
