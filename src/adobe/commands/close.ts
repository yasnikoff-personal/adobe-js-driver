import * as core from "./_core";

// Close connection
export class Command implements core.Command {
  kind = core.Kind.Close;
}
