import * as core from "./_core";
import * as loadScript from "./load_script";

export type Options = {
  namespace: string;
  functionName: string;
  functionArgs: any;
};

export const defaultExecuteOptions = {
  namespace: loadScript.defaultOptions.namespace,
  functionName: "main",
  functionArgs: {}
};

// Execute Script
export class Command implements core.Command<Options> {
  kind = core.Kind.Execute;
  options: Options;
  constructor(options?: Partial<Options>) {
    this.options = { ...this.default(), ...options };
  }
  default(): Options {
    return defaultExecuteOptions;
  }
}
