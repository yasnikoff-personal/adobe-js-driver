export interface ResultOrError<R = null> {
  result?: R;
  error?: Error;
}

export enum Kind {
  Empty = "", // fixme remove this workaround
  Close = "close",
  LoadScript = "load_script",
  Execute = "exec",
  Echo = "echo",
  OpenDoc = "open_doc",
  CloseDoc = "close_doc"
}

export function getType(typeName: string): Kind {
  switch (typeName) {
    case "":
      return Kind.Empty;
    case "close":
      return Kind.Close;
    case "load_script":
      return Kind.LoadScript;
    case "exec":
      return Kind.Execute;
    case "echo":
      return Kind.Echo;
    case "open_doc":
      return Kind.OpenDoc;
    case "close_doc":
      return Kind.CloseDoc;
    default:
      throw new Error(`unknown command type: "${typeName}"`);
  }
}

export const kindFieldName = "kind";
export const optionsFieldName = "options";

export interface Command<Options = null, Result = null> {
  readonly [kindFieldName]: Kind;
  readonly [optionsFieldName]?: Options; // 'arguments' conflicts with constructor's 'arguments' property
  result?: ResultOrError<Result>;
}
