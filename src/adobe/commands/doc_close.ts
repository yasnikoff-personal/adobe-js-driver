import * as core from "./_core";

// Close Document
export class Command implements core.Command {
  public kind = core.Kind.CloseDoc;
}
