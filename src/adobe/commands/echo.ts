import * as core from "./_core";

// Echo value
export class Options<T> {
  constructor(readonly value: T) {}
}
// echo command is kind of stepping stone for development:
// its primary purpose is to make sure we can pass values back and forth unchanged.
export class Command<T = null> implements core.Command<Options<T>, T> {
  kind = core.Kind.Echo;
  constructor(readonly options: Options<T>) {}
}
