import * as core from "./_core";

export type Options = {
  path: string;
};

// Open Document
export class Command implements core.Command<Options> {
  public kind = core.Kind.OpenDoc;
  constructor(readonly options: Options) {}
}

export class OpenDocError extends Error {
  private static errMsg(options?: Options): string {
    const path = options && options.path ? ` '${options.path}'` : "";
    return `can't open document ${path}`;
  }
  constructor(readonly options?: Options, readonly cause?: Error) {
    super(`${OpenDocError.errMsg(options)}:${cause}`);
  }
}
