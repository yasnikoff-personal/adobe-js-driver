import winston from "winston";

export type Logger = winston.Logger;

export function getDefaultLogger(): Logger {
  return winston.createLogger({
    transports: [new winston.transports.Console()]
  });
}
